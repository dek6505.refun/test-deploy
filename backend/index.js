const express = require('express')
const app = express()
let PORT = process.env.PORT || 5000
app.get('/', (req, res) => {
  res.send('Hello World')
})

app.listen(PORT, () => {
  console.log('Start server at port 5000.')
})